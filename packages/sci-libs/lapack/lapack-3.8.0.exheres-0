# Copyright 2010 Elias Pipping <pipping@exherbo.org>
# Copyright 2010 Cecil Curry <leycec@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require cmake [ api=2 ] alternatives

SUMMARY="Linear Algebra Package"
HOMEPAGE="http://www.netlib.org/${PN}"
DOWNLOADS="${HOMEPAGE}/${PNV}.tar.gz"

LICENCES="BSD-3"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        sys-libs/libgfortran:=
        virtual/blas
"

CMAKE_SRC_CONFIGURE_PARAMS=(
    # link to libblas.so for alternatives handling
    -DBLAS_LIBRARIES:STRING=blas
    -DBUILD_DEPRECATED:BOOL=FALSE
    -DBUILD_COMPLEX:BOOL=TRUE
    -DBUILD_COMPLEX16:BOOL=TRUE
    -DBUILD_DOUBLE:BOOL=TRUE
    -DBUILD_SHARED_LIBS:BOOL=TRUE
    -DBUILD_SINGLE:BOOL=TRUE
    -DCMAKE_Fortran_COMPILER=${FORTRAN}
)
CMAKE_SRC_CONFIGURE_TESTS=(
    '-DBUILD_TESTING:BOOL=TRUE -DBUILD_TESTING:BOOL=FALSE'
)

src_install() {
    cmake_src_install

    # Do not install .cmake files
    edo rm -rf "${IMAGE}"/usr/$(exhost --target)/lib/cmake

    # Name of LAPACK library to be installed.
    LAPACK_LIBRARY="liblapack-netlib.so"
    edo mv "${IMAGE}"/usr/$(exhost --target)/lib/{liblapack.so,${LAPACK_LIBRARY}}
    alternatives_for lapack lapack-netlib \
        0 /usr/$(exhost --target)/lib/liblapack.so ${LAPACK_LIBRARY}
}

