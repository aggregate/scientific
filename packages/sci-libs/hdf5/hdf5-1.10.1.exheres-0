# Copyright 2011 Elias Pipping <pipping@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require autotools [ supported_autoconf=[ 2.5 ] supported_automake=[ 1.15 ] ]

MY_PNV=${PNV/_p/-patch}
WORK="${WORKBASE}"/${MY_PNV}

SUMMARY="Library for storing and managing data"
HOMEPAGE="https://support.hdfgroup.org/HDF5"
DOWNLOADS="https://support.hdfgroup.org/ftp/HDF5/releases/hdf5-$(ever range 1-2)/${MY_PNV}/src/${MY_PNV}.tar.bz2"

UPSTREAM_DOCUMENTATION="${HOMEPAGE}/doc$(ever range 1-2)"
UPSTREAM_RELEASE_NOTES="https://support.hdfgroup.org/ftp/HDF5/releases/hdf5-$(ever range 1-2)/${MY_PNV}/src/${MY_PNV}-RELEASE.txt"

LICENCES="${PN}"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    fortran [[ description = [ Build fortran libraries ] ]]
    mpi
"

DEPENDENCIES="
    build:
        fortran? ( sys-libs/libgfortran:* )
    build+run:
        mpi? ( sys-cluster/openmpi[fortran=] )
        sys-libs/zlib
"

DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/0001-build-Respect-datarootdir.patch
)

DEFAULT_SRC_TEST_PARAMS=( TIME=echo )

src_configure() {
    # MPI is incompatible with both shared libraries and C++ bindings
    # however some may still need it
    local myconf=(
        --enable-build-mode=production
        --enable-deprecated-symbols
        --enable-internal-debug=none
        --enable-optimization=none
        --enable-profiling=no
        --enable-symbols=no
        --disable-asserts
        --disable-codestack
        --disable-developer-warnings
        --disable-instrument
        --disable-java
        --disable-trace
        $(option_enable fortran)
        $(option_enable mpi parallel)
        $(option_enable mpi static)
        $(option_enable !mpi cxx)
        $(option_enable !mpi shared)
    )

    if option mpi; then
        CC=mpicc
        CXX=mpic++
        FC=mpif77
        CFLAGS="${CFLAGS} -fPIC"
        CXXFLAGS="$${CXXFLAGS} -fPIC"
        FFLAGS="${FFLAGS} -fPIC"
    fi

    CC="${CC}" CXX="${CXX}" FC="${FC}" CFLAGS="${CFLAGS}" CXXFLAGS="${CXXFLAGS}" FFLAGS="${FFLAGS}" \
        econf "${myconf[@]}"
}

